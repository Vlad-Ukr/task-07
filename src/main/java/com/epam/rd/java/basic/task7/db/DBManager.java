package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    public static final String CONNECTION_URL = "connection.url";
    private PreparedStatement stmt;

    private ResultSet rs;

    private Connection con;

    private Properties prop;

    private Connection transCon;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        FileInputStream fileInputStream;
        prop = new Properties();
        try {
            fileInputStream = new FileInputStream("app.properties");
            prop.load(fileInputStream);
        } catch (IOException e) {
            new DBException("load properties error", e);
        }
    }

    public Connection getConnection(String connectionUrl) throws SQLException {
        return DriverManager.getConnection(connectionUrl);
    }

    public List<User> findAllUsers() throws DBException {
        List<User> result = new ArrayList<>();
        try {
            String query = "select id,login from users";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(new User(rs.getInt("id"), rs.getString("login")));
            }

        } catch (SQLException e) {
            new DBException("findAllUsers error", e);
        } finally {
            tryToClose();
        }
        return result;
    }

    public boolean insertUser(User user) throws DBException {
        try {
            String query = "INSERT INTO users(login) VALUES (?)";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            stmt.setString(1, user.getLogin());

            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            tryToClose();
        }
        user.setId(getUser(user.getLogin()).getId());
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        if (users == null) {
            return false;
        }

        try {
            for (User user : users) {
                String query = "delete from users where name = ?";
                con = getConnection(prop.getProperty(CONNECTION_URL));

                stmt = con.prepareStatement(query);
                stmt.setString(1, user.getLogin());

                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            new DBException("deleteTeam", e);
            return false;
        } finally {
            tryToClose();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User result = new User(login);
        try {
            String query = "select id from users where login = ?";
            con = getConnection(prop.getProperty(CONNECTION_URL));
            stmt = con.prepareStatement(query);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.setId(rs.getInt("id"));
                break;
            }
        } catch (SQLException e) {
            new DBException("getUser", e);
        } finally {
            tryToClose();
        }
        return result;
    }

    public Team getTeam(String name) throws DBException {
        Team result = new Team(name);
        try {
            String query = "select id from teams where name = ?";
            con = getConnection(prop.getProperty(CONNECTION_URL));
            stmt = con.prepareStatement(query);
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.setId(rs.getInt("id"));
                break;
            }
        } catch (SQLException e) {
            new DBException("getTeam", e);
        } finally {
            tryToClose();
        }
        return result;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> result = new ArrayList<>();
        try {
            String query = "select id,name from teams";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(new Team(rs.getInt("id"), rs.getString("name")));
            }

        } catch (SQLException e) {
            new DBException("findAllTeams", e);
        } finally {
            tryToClose();
        }
        return result;
    }

    public boolean insertTeam(Team team) throws DBException {
        try {
            String query = "INSERT INTO teams(name) VALUES (?)";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            stmt.setString(1, team.getName());

            stmt.executeUpdate();
        } catch (SQLException e) {
            new DBException("insert team error", e);
            return false;
        } finally {
            tryToClose();
        }
        team.setId(getTeam(team.getName()).getId());
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {

        try {
            String query = "INSERT INTO users_teams(user_id,team_id) VALUES (?,?) ";

            transCon = getConnection(prop.getProperty(CONNECTION_URL));
            transCon.setAutoCommit(false);

            if (user == null) {
                throw new SQLException("User doesnt exist");
            }

            for (Team team1 : teams) {

                User u = getUser(user.getLogin());

                if (team1 == null) {
                    continue;
                }
                Team t = getTeam(team1.getName());

                stmt = transCon.prepareStatement(query);
                stmt.setInt(1, u.getId());
                stmt.setInt(2, t.getId());
                stmt.executeUpdate();
            }

            transCon.commit();
        } catch (SQLException e) {
            try {
                transCon.rollback();
                throw  new DBException("k",e);
            } catch (SQLException sqlException) {
              throw  new DBException("k",sqlException);

            }

        } finally {
            try {
                transCon.close();
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> result = new ArrayList<>();
        List<Integer> idList = new ArrayList<>();
        try {
            String query = "select team_id from users_teams where user_id = ?";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            stmt.setInt(1, user.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
                idList.add(rs.getInt("team_id"));
            }

            for (Integer integer : idList) {
                query = "select id,name from teams where id = ?";

                stmt = con.prepareStatement(query);
                stmt.setInt(1, integer);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    result.add(new Team(rs.getInt("id"), rs.getString("name")));
                }


            }

        } catch (SQLException e) {
            new DBException("getUserTeams", e);
        } finally {
            tryToClose();
        }
        return result;
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }

        try {
            String query = "delete from teams where name = ?";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            stmt.setString(1, team.getName());

            stmt.executeUpdate();
        } catch (SQLException e) {
            new DBException("deleteTeam", e);
            return false;
        } finally {
            tryToClose();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try {
            String query = "update teams set name = ? where id = ?";
            con = getConnection(prop.getProperty(CONNECTION_URL));

            stmt = con.prepareStatement(query);
            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());

            stmt.executeUpdate();
        } catch (SQLException e) {
            new DBException("updateTeam", e);
            return false;
        } finally {
            tryToClose();
        }
        return true;
    }

    public void tryToClose() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }
}
